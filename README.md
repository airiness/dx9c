# The Project Of DirectX9.0 - Plane Game
***
 the game concept is like
[『斑鳩 IKARUGA』](https://ja.wikipedia.org/wiki/斑鳩_(シューティングゲーム) "wiki page")<br>
get same color bullets can ge score.<br>
use different color bullets to attack enemies.<br>
made using DirectX9.0.<br>

## operate method:
Keyboard:<br> 
Left`A` Right`D` Up`W` Down`S`<br>
Shoot`Space`<br> 
Change into blue coloe`Q`<br>
Change into red color`E`<br>

## screenshots:
***
title:<br>
![](images/dx9c.png)
***
in game:<br>
![](images/dx9cIngame.png)
***
game over scene:<br>
![](images/dx9cGameOver.png)