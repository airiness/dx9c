#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "enemy.h"
#include "player.h"
#include "bullet.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"

const size_t g_enemy_type_num = 4;
const D3DXVECTOR2 g_enemy_default_size = { 64.0f,64.0f };
unsigned int g_enemy_textureID[g_enemy_type_num];
ENEMY g_enemies[ENEMY_MAX];

D3DCOLOR g_enemy_red = D3DCOLOR_RGBA(150, 0, 0, 255);
D3DCOLOR g_enemy_blue = D3DCOLOR_RGBA(0, 0, 150, 255);

int g_enemy_frames;
//struct EnemyMake
//{
//	int enemyMakeBeginFrame;
//	D3DXVECTOR2 enemyMakeBirthPosition;
//	float enemyMakeSpeed;
//	D3DXVECTOR2 enemyMakeDirection;
//	ENEMY_TYPE enemyMakeType;
//	int enemyMakeHP;
//	int enemyMakeBulletType;
//	int enemyMakeLifeFrameNum;
//	int enemyMakeBulletCD;
//	bool enemyMakeFinished;
//};
EnemyMake g_enemies_make_list[]=
{
	{20, {-100.0f,80.0f},2.0f,{1.0f,0.2f},EnemyFishA,10,0,1000,30,false},
	{20, {-300.0f,80.0f},2.0f,{1.0f,0.2f},EnemyFishB,10,1,1000,30,false},
	{20, {-500.0f,80.0f},2.0f,{1.0f,0.2f},EnemyFishC,10,0,1000,30,false},
	{20, {-700.0f,80.0f},2.0f,{1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	{222,{250.0f, -100.0f},2.0f,{-0.1f,0.2f},EnemyFishB,20,0,1000,30,false},
	{223,{500.0f, -100.0f},2.2f,{0.1f,0.2f},EnemyFishC,20,0,1000,30,false},
	{223,{750.0f, -100.0f},2.0f,{-0.1f,0.2f},EnemyFishC,20,0,1000,30,false},
	{223,{1000.0f,-100.0f},2.0f,{0.2f,0.2f},EnemyFishC,20,1,1000,30,false},
	{223,{-100.0f,-100.0f},2.0f,{0.1f,0.2f},EnemyFishC,20,1,1000,30,false},
	{500,{1300.0f,0.0f},2.0f,{-1.0f,0.2f},EnemyFishC,10,0,1000,30,false},
	{500,{1400.0f,0.0f},3.0f,{-1.0f,0.2f},EnemyFishC,10,0,1000,30,false},
	{500,{1600.0f,0.0f},2.0f,{-1.0f,0.2f},EnemyFishA,10,0,1000,30,false},
	{500,{1700.0f,0.0f},1.0f,{-1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	{500,{1900.0f,0.0f},2.0f,{-1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	{700,{1300.0f,0.0f},2.0f,{-1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	{700,{100.0f,-100.0f},2.0f,{0.0f,1.0f},EnemyFishC,10,0,1000,30,false},
	{700,{300.0f,-100.0f},2.2f,{0.0f,1.0f},EnemyFishC,10,0,1000,30,false},
	{700,{500.0f,-100.0f},2.3f,{0.0f,1.0f},EnemyFishC,10,0,1000,30,false},
	{700,{700.0f,-100.0f},1.0f,{0.0f,1.0f},EnemyFishC,10,0,1000,30,false},
	{900,{900.0f,-100.0f},2.0f,{0.0f,1.0f},EnemyFishC,10,0,1000,30,false},
	{900,{1200.0f,200.0f},2.0f,{0.0f,1.0f},EnemyFishB,10,1,1000,30,false},
	{900,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishB,10,1,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishB,10,1,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishB,10,1,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishB,10,1,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishC,10,0,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishC,10,0,1000,30,false},
	//{1300,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishC,10,1,1000,30,false},
	//{1600,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishC,10,1,1000,30,false},
	//{1600,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	//{1600,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishA,10,1,1000,30,false},
	//{1600,{500.0f,200.0f},2.0f,{1.0f,0.2f},EnemyFishA,10,0,1000,30,false},
	{2200,{500.0f,200.0f},2.0f,{0.0f,0.0f},EnemyBoss,200,1,10000000,30,false},
};

extern D3DXVECTOR2 g_player_position;

void enemyInit()
{
	g_enemy_textureID[0] = Texture_SetLoadFile("Asset/Texture/plane2.png", 64,64);
	g_enemy_textureID[1] = Texture_SetLoadFile("Asset/Texture/plane3.png", 64,64);
	g_enemy_textureID[2] = Texture_SetLoadFile("Asset/Texture/plane4.png", 64,64);
	g_enemy_textureID[3] = Texture_SetLoadFile("Asset/Texture/plane5.png", 64,64);
	enemyReset();
}

void enemyUninit() {

}

void enemyUpdate()
{
	g_enemy_frames++;

	for (auto & e: g_enemies_make_list)
	{
		if (!e.enemyMakeFinished && e.enemyMakeBeginFrame < g_enemy_frames)
		{
			enemyCreate(e.enemyMakeBirthPosition, e.enemyMakeSpeed,
				e.enemyMakeDirection, e.enemyMakeType,e.enemyMakeBulletType, e.enemyMakeBulletCD,
				e.enemyMakeHP, e.enemyMakeLifeFrameNum);
			e.enemyMakeFinished = true;
		}
	}


	for (size_t i = 0; i < ENEMY_MAX; i++)
	{
		if (g_enemies[i].enemyIsUse)
		{
			switch (g_enemies[i].enemyType)
			{
			case EnemyFishA:
				break;
			case EnemyFishB:
				break;
			case EnemyFishC:
				break;
			case EnemyBoss:
				break;
			default:
				break;
			}

			g_enemies[i].enemyBulletCDing++;
			if (g_enemies[i].enemyBulletCDing > g_enemies[i].enemyBulletCD)
			{
				D3DXVECTOR2 dir = g_player_position - g_enemies[i].enemyPosition;
				D3DXVec2Normalize(&dir, &dir);
				bulletCreate(g_enemies[i].enemyPosition, dir, Bullet_type_line, Bullet_belong_enemy, 3.0f, g_enemies[i].enemyBulletType, 1);
				g_enemies[i].enemyBulletCDing = 0;
			}

			D3DXVec2Normalize(&g_enemies[i].enemyDirection, &g_enemies[i].enemyDirection);
			g_enemies[i].enemyPosition += g_enemies[i].enemyDirection * g_enemies[i].enemySpeed;

			g_enemies[i].enemyLifeFraming++;
			if (g_enemies[i].enemyLifeFraming > g_enemies[i].enemyLifeFrame)
			{
				g_enemies[i].enemyIsUse = false;
				g_enemies[i].enemyLifeFraming = 0;
			}

			if (g_enemies[i].enemyHP<=0)
			{
				g_enemies[i].enemyIsUse = false;
				g_enemies[i].enemyHP = 0;
			}
		}
	}
}

void enemyDraw() {
	for (int i = 0; i < ENEMY_MAX; i++) {
		if (g_enemies[i].enemyIsUse == true)
		{
			if (g_enemies[i].enemyBulletType == 0)
			{
				SetColor(g_enemy_red);
			}
			else if (g_enemies[i].enemyBulletType == 1)
			{
				SetColor(g_enemy_blue);
			}
			Sprite_Draw(g_enemies[i].enemyTextureID, g_enemies[i].enemyPosition.x, g_enemies[i].enemyPosition.y,180.0f);
		}
	}
	SetColor(D3DCOLOR_RGBA(255, 255, 255, 255));
}

void enemyReset()
{
	g_enemy_frames = 0;
	for (auto & e : g_enemies_make_list)
	{
		e.enemyMakeFinished = false;
	}
	for (int i = 0; i < ENEMY_MAX; i++)
	{
		g_enemies[i].enemyIsUse = false;
		g_enemies[i].enemyPosition = { 0.0f,0.0f };
		g_enemies[i].enemySize = g_enemy_default_size;
		g_enemies[i].enemySpeed = 0.0f;
		g_enemies[i].enemyType = EnemyFishA;
		g_enemies[i].enemyTextureID = g_enemy_textureID[0];
		g_enemies[i].enemyBulletType = 0;
		g_enemies[i].enemyBulletCD = 0;
		g_enemies[i].enemyBulletCDing = 0;
		g_enemies[i].enemyHP = 0;
		g_enemies[i].enemyLifeFrame = 0;
		g_enemies[i].enemyLifeFraming = 0;
	}
}

//struct ENEMY
//{
//	bool enemyIsUse;
//	D3DXVECTOR2 enemyPosition;
//	D3DXVECTOR2 enemySize;
//	D3DXVECTOR2 enemySpeed;
//	ENEMY_TYPE enemyType;
//	unsigned int enemyTextureID;
//	int enemyBulletType;
//	int enemyBulletCD;
//	int enemyBulletCDing;
//	int enemyHP;
//	int enemyLifeFrame;
//  int enemyLifeFraming;
//};

void enemyCreate(D3DXVECTOR2 position, float speed, D3DXVECTOR2 direction, ENEMY_TYPE type,
	int bullettype,int enemybulletCD, int hp, int lifeframe)
{
	for (int i = 0; i < ENEMY_MAX; i++) {
		if (g_enemies[i].enemyIsUse == false)
		{
			g_enemies[i].enemyIsUse = true;
			g_enemies[i].enemyPosition = position;
			g_enemies[i].enemySize = g_enemy_default_size;
			g_enemies[i].enemySpeed = speed;
			g_enemies[i].enemyDirection = direction;
			g_enemies[i].enemyType = type;
			g_enemies[i].enemyTextureID = g_enemy_textureID[type];
			g_enemies[i].enemyBulletType = bullettype;
			g_enemies[i].enemyBulletCD = enemybulletCD;
			g_enemies[i].enemyBulletCDing = 0;
			g_enemies[i].enemyLifeFrame = lifeframe;
			g_enemies[i].enemyLifeFraming = 0;
			g_enemies[i].enemyHP = hp;
			break;
		}
	}
}
