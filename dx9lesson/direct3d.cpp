#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "direct3d.h"
#include "main.h"

static LPDIRECT3D9 g_D3D = nullptr;
LPDIRECT3DDEVICE9 g_pDevice = nullptr;
D3DPRESENT_PARAMETERS d3dpp = {};

bool Direct3DInit(HWND hWnd) 
{

	d3dpp.BackBufferWidth = SCREEN_WIDTH;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_FLIP;
	d3dpp.Windowed = TRUE;								
	d3dpp.EnableAutoDepthStencil = TRUE;				
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;			
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT; //Also have _ONE, _IMMEDIATE

	g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (!g_D3D) {
		MessageBox(NULL, "Missing g_D3D, Direct3DCreate9(D3D_SDK_VERSION)", "Missing", MB_OK | MB_ICONINFORMATION);
	}

	HRESULT hr = g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &g_pDevice);
	if (FAILED(hr)) {	//hrが失敗だったら真
		MessageBox(NULL, "hr failure", "Failure", MB_OK | MB_ICONINFORMATION);
		return false;
	}

	return true;
}

void Direct3DUninit() 
{
	if (g_pDevice) {
		g_pDevice->Release();
	}
}

LPDIRECT3DDEVICE9 Direct3DGetDevice() 
{
	return g_pDevice;
}

void Direct3DBeginScene()
{
	g_pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_RGBA(0, 0, 0, 255), 1.0f, 0);
	g_pDevice->BeginScene();
	g_pDevice->SetFVF(FVF_VERTEX2D);
}

void Direct3DEndScene()
{
	g_pDevice->EndScene();	
	g_pDevice->Present(NULL, NULL, NULL, NULL);
}
