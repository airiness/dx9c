#pragma once

#define FVF_FADE_VERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)

typedef struct FadeVertex_tag
{
	D3DXVECTOR4 position;
	D3DCOLOR color;
}FadeVertex;

void fadeInit();
bool fadeUpdate();
void fadeUninit();
void fadeStart(int frame, D3DCOLOR color, bool bOut);
void fadeDraw();
