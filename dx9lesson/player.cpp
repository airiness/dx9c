﻿#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <algorithm>
#include "main.h"
#include "game.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "input.h"
#include "bullet.h"
#include "effect.h"
#include "player.h"

D3DXVECTOR2 g_player_position;
D3DXVECTOR2 g_player_size;
bool g_player_isUse;
int g_player_bullet_type;

D3DCOLOR g_player_effect_color;
D3DCOLOR g_player_red = D3DCOLOR_RGBA(225, 0, 0, 255);
D3DCOLOR g_player_blue = D3DCOLOR_RGBA(0, 0, 225, 255);

size_t g_player_textureID;
const D3DXVECTOR2 g_player_texture_size = { 64.0f,64.0f };
const float g_player_speed = 10.0f;
int g_player_damage = 1;

extern D3DXVECTOR2 g_effect_size;

void playerInit()
{
	g_player_size = g_player_texture_size;
	g_player_textureID = Texture_SetLoadFile("Asset/Texture/plane2.png", g_player_size.x, g_player_size.y);
	playerReset();
}

void playerUninit()
{

}

void playerUpdate()
{
	if (g_player_isUse)
	{
		D3DXVECTOR2 dir(0.0f, 0.0f);
		static unsigned int cd = 10;
		if (cd != 0)
		{
			cd--;
		}
		g_effect_size = g_player_size;

		if (Keyboard_IsTrigger(DIK_Q))
		{
			g_player_bullet_type = 0;
			g_player_effect_color = D3DCOLOR_RGBA(255, 25, 25, 255);
		}
		else if (Keyboard_IsTrigger(DIK_E))
		{
			g_player_bullet_type = 1;
			g_player_effect_color = D3DCOLOR_RGBA(25, 25, 255, 255);
		}

		if (Keyboard_IsPress(DIK_W))
		{
			dir.y -= 1.2f;

			effectCreate(g_player_position.x, g_player_position.y, g_player_effect_color, 20, 1.0f);

		}
		if (Keyboard_IsPress(DIK_A))
		{
			dir.x -= 1.2f;
			effectCreate(g_player_position.x, g_player_position.y, g_player_effect_color, 20, 1.0f);

		}
		if (Keyboard_IsPress(DIK_S))
		{
			dir.y += 1.2f;
			effectCreate(g_player_position.x, g_player_position.y, g_player_effect_color, 20, 1.0f);

		}
		if (Keyboard_IsPress(DIK_D))
		{
			dir.x += 1.2f;
			effectCreate(g_player_position.x, g_player_position.y, g_player_effect_color, 20, 1.0f);

		}
		if (Keyboard_IsPress(DIK_SPACE))
		{

			if (cd == 0)
			{
				bulletCreate(g_player_position, { 0.0f,-1.0f }, Bullet_type_follow, Bullet_belong_player, 15.0f, g_player_bullet_type, g_player_damage);
				cd = 10;
			}
		}


		D3DXVec2Normalize(&dir, &dir);
		g_player_position += dir * g_player_speed;

		g_player_position.x = (std::min)(float(SCREEN_WIDTH) - g_player_size.x / 2.0f, g_player_position.x);
		g_player_position.x = (std::max)(g_player_size.x / 2.0f, g_player_position.x);
		g_player_position.y = (std::min)(float(SCREEN_HEIGHT) - g_player_size.y / 2.0f, g_player_position.y);
		g_player_position.y = (std::max)(g_player_size.y / 2.0f, g_player_position.y);
	}
	else
	{
		static int useCD = 0;
		useCD++;
		if (useCD > 10)
		{
			SetGameState(STATE_RESULT);
			useCD = 0;
		}
	}


}

void playerDraw()
{
	if (g_player_isUse)
	{
		if (g_player_bullet_type == 0)
		{
			SetColor(g_player_red);
		}
		else if (true)
		{
			SetColor(g_player_blue);
		}
		Sprite_Draw(g_player_textureID, g_player_position.x, g_player_position.y);
		SetColor(D3DCOLOR_RGBA(255, 255, 255, 255));
	}
}

void playerReset()
{
	g_player_effect_color = D3DCOLOR_RGBA(255, 255, 255, 255);
	g_player_isUse = true;
	g_player_bullet_type = 0;
	g_player_position = D3DXVECTOR2(SCREEN_WIDTH * 0.5f, SCREEN_HEIGHT * 0.8f);
}

float GetPlayerSpeed()
{
	return g_player_speed;
}

D3DXVECTOR2 & GetPlayerPosition()
{
	return g_player_position;
}

D3DXVECTOR2 & GetPlayerSize()
{
	return g_player_size;
}
