#pragma once

enum PlayerState
{
	PlayerBlue,
	PlayerRed,
};


void playerInit();
void playerUninit();
void playerUpdate();
void playerDraw();
void playerReset();
float GetPlayerSpeed();
D3DXVECTOR2& GetPlayerPosition();
D3DXVECTOR2& GetPlayerSize();


