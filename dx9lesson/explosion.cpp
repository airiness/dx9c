#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "enemy.h"
#include "player.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "explosion.h"

size_t	g_explosion_textureID;
EXPLOSION	explosions[EXPLOSION_MAX];

D3DXVECTOR2 g_explosion_size;
const D3DXVECTOR2 g_explosion_texture_size = {256.0f,256.0f};

void explosionInit()
{
	g_explosion_textureID = Texture_SetLoadFile("Asset/Texture/exp.png", g_explosion_texture_size.x, g_explosion_texture_size.y);
	explosionReset();
}

void explosionUninit()
{

}

void explosionUpdate()
{
	static unsigned int cd = 5;
	for (int i = 0; i < EXPLOSION_MAX; i++)
	{
		if (explosions[i].isUse == true)
		{
			if (cd == 0)
			{
				explosions[i].Frame++;
				if (explosions[i].Frame > 8)
				{
					explosions[i].Frame = 0;
					explosions[i].isUse = false;
				}
				cd = 5;
			}
			else
			{
				cd--;
			}
		}
	}
}



void explosionDraw()
{

	for (size_t i = 0; i < EXPLOSION_MAX; i++)
	{
		if (explosions[i].isUse) {
			Sprite_Draw(g_explosion_textureID, explosions[i].dx, explosions[i].dy,
				(g_explosion_size.x / 4.f)*((explosions[i].Frame) % 4),
				(g_explosion_size.y / 4.f)*((explosions[i].Frame) / 4),
				(g_explosion_size.x / 4.f), (g_explosion_size.y / 4.f));
		}		
	}

}

void explosionReset()
{
	g_explosion_size = g_explosion_texture_size;
	for (size_t i = 0; i < EXPLOSION_MAX; i++)
	{
		explosions[i].dx = 0.f;
		explosions[i].dy = 0.f;
		explosions[i].Frame = 0;
		explosions[i].isUse = false;
	}
}


void SetExplosion(float x, float y)
{

	for (int i = 0; i < EXPLOSION_MAX; i++)
	{
		if (explosions[i].isUse == false)
		{
			explosions[i].dx = x;
			explosions[i].dy = y;
			explosions[i].isUse = true;
			explosions[i].Frame = 0;
			break;
		}
	}

}
