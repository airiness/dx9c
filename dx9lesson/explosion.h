#pragma once

#define EXPLOSION_MAX 100

struct EXPLOSION
{
	float	dx, dy;
	bool	isUse;
	int Frame;
};


void explosionInit();
void explosionUninit();
void explosionUpdate();
void explosionDraw();
void explosionReset();

void SetExplosion(float x, float y);