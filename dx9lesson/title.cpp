#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "main.h"
#include "game.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "input.h"
#include "title.h"
#include "fade.h"

static D3DXVECTOR2 g_title_position;
static D3DXVECTOR2 g_title_size;
static int g_title_textureID;

void titleInit()
{
	g_title_textureID = Texture_SetLoadFile("Asset/Texture/title.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	Texture_Load();
	g_title_position = { SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.5 };
	g_title_size = { SCREEN_WIDTH, SCREEN_HEIGHT };
}

void titleUpdate()
{
	static float t = 0.0f;
	t += 1.0f / 60.0f;
	if (t>10.0f)
	{
		fadeStart(30, D3DCOLOR_RGBA(55, 255, 255, 255), false);

		SetGameState(STATE_HIGHSCORE);
		t = 0.0f;
	}

	if (Keyboard_IsTrigger(DIK_SPACE))
	{
		fadeStart(30, D3DCOLOR_RGBA(55, 255, 255, 255),false);

		SetGameState(STATE_INGAME);

	}
}

void titleUninit()
{

}

void titleDraw()
{
	Sprite_Draw(g_title_textureID, g_title_position.x, g_title_position.y);

}
