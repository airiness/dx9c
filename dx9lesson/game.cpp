﻿#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "game.h"
#include "main.h"
#include "input.h"
#include "system_timer.h"
#include "debug_font.h"
#include "texture.h"
#include "direct3d.h"
#include "player.h"
#include "bullet.h"
#include "enemy.h"
#include "collision.h"
#include "explosion.h"
#include "score.h"
#include "background.h"
#include "fade.h"
#include "title.h"
#include "result.h"
#include "effect.h"
#include "highscore.h"

int g_FrameCount;
int g_FPSBaseFrameCount;
double g_FPSBaseTime;
float g_FPS;
GameState g_game_state;

void gameInit() {
	DebugFont_Initialize();
	SystemTimer_Initialize();
	SystemTimer_Start();

	g_FrameCount = g_FPSBaseFrameCount = 0;
	g_FPSBaseTime = SystemTimer_GetTime();
	g_FPS = 0.0;

	g_game_state = STATE_TITLE;

	titleInit();
	highscoreInit();
	resultInit();
	fadeInit();
	backgroundInit();
	scoreInit();
	playerInit();
	bulletInit();
	enemyInit();
	explosionInit();
	effectInit();

	Texture_Load();

	LPDIRECT3DDEVICE9 myDevice = Direct3DGetDevice();
	myDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	myDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	myDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	myDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	myDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	myDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

void gameUninit() 
{
	effectUninit();
	fadeUninit();
	backgroundUninit();
	explosionUninit();
	bulletUninit();
	playerUninit();
	enemyUninit();
	scoreUninit();
	titleUninit();
	resultUninit();
}

void gameUpdate() {

	g_FrameCount++;
	double time = SystemTimer_GetTime();

	if (time - g_FPSBaseTime >= FPS_MEASUREMENT_TIME) {
		g_FPS = (float)((g_FrameCount - g_FPSBaseFrameCount) / (time - g_FPSBaseTime));
		g_FPSBaseTime = time;
		g_FPSBaseFrameCount = g_FrameCount;
	}

	switch (g_game_state)
	{
	case STATE_NONE:
		break;
	case STATE_TITLE:
		titleUpdate();
		break;
	case STATE_INGAME:
		backgroundUpdate();
		playerUpdate();
		bulletUpdate();
		scoreUpdate();
		enemyUpdate();
		explosionUpdate();
		effectUpdate();
		break;
	case STATE_RESULT:
		resultUpdate();
		break;
	case STATE_HIGHSCORE:
		highscoreUpdate();
		break;
	default:
		break;
	}


}



void gameDraw()
{
	switch (g_game_state)
	{
	case STATE_NONE:
		break;
	case STATE_TITLE:
		titleDraw();
		break;
	case STATE_INGAME:
		backgroundDraw();
		effectDraw();
		playerDraw();
		bulletDraw();
		enemyDraw();
		explosionDraw();
		scoreDraw(7, true, true);
		break;
	case STATE_RESULT:
		resultDraw();
		break;
	case STATE_HIGHSCORE:
		highscoreDraw();
		break;
	default:
		break;
	}

	DebugFont_Draw(32, 32, "FPS:%.2f", g_FPS);
}

void gameReset()
{
	bulletReset();
	enemyReset();
	playerReset();
	scoreReset();
	explosionReset();
}

void SetGameState(GameState state)
{
	g_game_state = state;
}
