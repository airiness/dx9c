#pragma once

struct EFFECT
{
	float	dx, dy;
	int effectLife;
	int effectFrame;
	float effectScale;
	D3DCOLOR effectColor;
};

void effectInit();
void effectUninit();
void effectUpdate();
void effectDraw();
void effectReset();
void effectCreate(float x, float y, D3DCOLOR color, int life, float scale);