#pragma once
bool Direct3DInit(HWND hWnd);
void Direct3DUninit();
LPDIRECT3DDEVICE9 Direct3DGetDevice();

void Direct3DBeginScene();
void Direct3DEndScene();
