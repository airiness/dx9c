#pragma once

typedef struct Circle_tag{
	D3DXVECTOR2 position;
	float radius;
}Circle;

bool IsHitCollsion_Circle_Circle(const Circle* pA, const Circle* pB);