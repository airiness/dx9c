#pragma once
#define SCREEN_WIDTH	1280
#define SCREEN_HEIGHT	720
#define FVF_VERTEX2D	(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define FPS_MEASUREMENT_TIME (0.2)

typedef struct Vertex2d_tag {
	D3DXVECTOR4 position;
	D3DCOLOR color;
	D3DXVECTOR2 uv;			
}Vertex2d;
