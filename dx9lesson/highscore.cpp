#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "main.h"
#include "game.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "input.h"
#include "fade.h"
#include "highscore.h"

static D3DXVECTOR2 g_highscore_position;
static D3DXVECTOR2 g_highscore_size;
static int g_highscore_textureID;

void highscoreInit()
{
	g_highscore_textureID = Texture_SetLoadFile("Asset/Texture/highscore.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	Texture_Load();
	g_highscore_position = { SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.5 };
	g_highscore_size = { SCREEN_WIDTH, SCREEN_HEIGHT };
}
void highscoreUpdate()
{
	if (Keyboard_IsTrigger(DIK_SPACE))
	{
		fadeStart(30, D3DCOLOR_RGBA(55, 255, 255, 255), false);

		SetGameState(STATE_TITLE);

	}
}
void highscoreUninit()
{

}
void highscoreDraw()
{
	Sprite_Draw(g_highscore_textureID, g_highscore_position.x, g_highscore_position.y);
}
