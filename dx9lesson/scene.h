#pragma once

enum SCENE
{
	SCENE_TITLE,
	SCENE_GAME,
	SCENE_HIGHSCORE,
	SCENE_MAX,
	SCENE_END = SCENE_MAX,
};

void sceneInit();
void sceneUninit();
void sceneUpdate();
void sceneDraw();

void sceneSetNextScene(SCENE nextScene);
bool sceneChangeScene();