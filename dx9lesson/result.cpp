#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "main.h"
#include "game.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "input.h"
#include "result.h"
#include "fade.h"

static D3DXVECTOR2 g_result_position;
static D3DXVECTOR2 g_result_size;
static int g_result_textureID;

void resultInit()
{
	g_result_textureID = Texture_SetLoadFile("Asset/Texture/result.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	Texture_Load();
	g_result_position = { SCREEN_WIDTH * 0.5, SCREEN_HEIGHT * 0.5 };
	g_result_size = { SCREEN_WIDTH, SCREEN_HEIGHT };
}

void resultUpdate()
{
	if (Keyboard_IsTrigger(DIK_SPACE))
	{
		fadeStart(30, D3DCOLOR_RGBA(55, 255, 255, 255), false);
		SetGameState(STATE_TITLE);
		gameReset();
	}
}

void resultUninit()
{

}

void resultDraw()
{
	Sprite_Draw(g_result_textureID, g_result_position.x, g_result_position.y);
}
