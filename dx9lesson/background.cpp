#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "player.h"
#include "background.h"

static const size_t g_background_num = 3;

static D3DXVECTOR2 g_background_position[g_background_num];
static D3DXVECTOR2 g_background_size[g_background_num];
static size_t g_background_textureID[g_background_num];
static float g_background_speed[g_background_num];
static const float g_background_speed_step = 2.0f;

static bool isRight[g_background_num];
static bool isDown[g_background_num];

void backgroundInit()
{
	g_background_textureID[0] = Texture_SetLoadFile("Asset/Texture/background1.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	g_background_textureID[1] = Texture_SetLoadFile("Asset/Texture/background2.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	g_background_textureID[2] = Texture_SetLoadFile("Asset/Texture/background3.png", SCREEN_WIDTH, SCREEN_HEIGHT);
	Texture_Load();
	backgroundReset();
}

void backgroundUpdate()
{
	for (size_t i = 0; i < g_background_num; i++)
	{


		g_background_position[i].y += g_background_speed[i];




		while (g_background_position[i].x > (SCREEN_WIDTH * 1.5f))
		{
			g_background_position[i].x -= SCREEN_WIDTH;
		}
		while (g_background_position[i].x < (-SCREEN_WIDTH * 0.5f))
		{
			g_background_position[i].x += SCREEN_WIDTH;
		}

		while (g_background_position[i].y > (SCREEN_HEIGHT * 1.5f))
		{
			g_background_position[i].y -= SCREEN_HEIGHT;
		}
		while (g_background_position[i].y < (-SCREEN_HEIGHT * 0.5f))
		{
			g_background_position[i].y += SCREEN_HEIGHT;
		}

		if (g_background_position[i].x > SCREEN_WIDTH * 0.5f)
		{
			isRight[i] = true;
		}
		else if (g_background_position[i].x < SCREEN_WIDTH * 0.5f)
		{
			isRight[i] = false;
		}

		if (g_background_position[i].y > SCREEN_HEIGHT * 0.5f)
		{
			isDown[i] = true;
		}
		else if (g_background_position[i].y < SCREEN_HEIGHT * 0.5f)
		{
			isDown[i] = false;
		}
	}
}

void backgroundUninit()
{
}

void backgroundDraw()
{
	for (size_t i = 0; i < 3; i++)
	{
		float offsetX = isRight[i] ? -g_background_size[i].x : g_background_size[i].x;
		float offsetY = isDown[i] ? -g_background_size[i].y : g_background_size[i].y;
		Sprite_Draw(g_background_textureID[i], g_background_position[i].x, g_background_position[i].y);
		Sprite_Draw(g_background_textureID[i], g_background_position[i].x + offsetX, g_background_position[i].y);
		Sprite_Draw(g_background_textureID[i], g_background_position[i].x, g_background_position[i].y + offsetY);
		Sprite_Draw(g_background_textureID[i], g_background_position[i].x + offsetX, g_background_position[i].y + offsetY);
	}
}

void backgroundReset()
{
	for (size_t i = 0; i < g_background_num; i++)
	{
		g_background_size[i] = { SCREEN_WIDTH , SCREEN_HEIGHT };
		g_background_position[i] = { SCREEN_WIDTH * 0.5f + i * SCREEN_WIDTH - 20.0f, SCREEN_HEIGHT * 0.5f };
		g_background_speed[i] = (i + 1) * g_background_speed_step;

		isDown[i] = true;
		isRight[i] = true;
	}

}
