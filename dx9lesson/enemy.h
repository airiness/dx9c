#pragma once
#define ENEMY_MAX  (50)
enum ENEMY_TYPE
{
	EnemyFishA = 0,
	EnemyFishB = 1,
	EnemyFishC = 2,
	EnemyBoss = 3,
};

struct ENEMY
{
	bool enemyIsUse;
	D3DXVECTOR2 enemyPosition;
	D3DXVECTOR2 enemySize;
	float enemySpeed;
	D3DXVECTOR2 enemyDirection;
	ENEMY_TYPE enemyType;
	unsigned int enemyTextureID;
	int enemyBulletType;
	int enemyBulletCD;
	int enemyBulletCDing;
	int enemyHP;
	int enemyLifeFrame;
	int enemyLifeFraming;
};

struct EnemyMake
{
	int enemyMakeBeginFrame;
	D3DXVECTOR2 enemyMakeBirthPosition;
	float enemyMakeSpeed;
	D3DXVECTOR2 enemyMakeDirection;
	ENEMY_TYPE enemyMakeType;
	int enemyMakeHP;
	int enemyMakeBulletType;
	int enemyMakeLifeFrameNum;
	int enemyMakeBulletCD;
	bool enemyMakeFinished;
};

void enemyInit();
void enemyUninit();
void enemyUpdate();
void enemyDraw();
void enemyReset();
void enemyCreate(D3DXVECTOR2 position, float speed, D3DXVECTOR2 direction, ENEMY_TYPE type, int bullettype,
	int enemybulletCD, int hp, int lifeframe);
