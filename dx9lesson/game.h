#pragma once

enum GameState
{
	STATE_NONE,
	STATE_TITLE,
	STATE_INGAME,
	STATE_RESULT,
	STATE_HIGHSCORE,

};

void gameInit();
void gameUninit();
void gameUpdate();
void gameDraw();
void gameReset();

void SetGameState(GameState state);