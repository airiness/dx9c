#pragma once
#define BULLET_MAX (200)
enum BULLET_TYEP
{
	Bullet_type_none,
	Bullet_type_line,
	Bullet_type_sin,
	Bullet_type_follow,
};

enum BULLET_BELONG
{
	Bullet_belong_noone,
	Bullet_belong_player,
	Bullet_belong_enemy,
};

struct BULLET 
{
	bool bulletIsUse;
	D3DXVECTOR2 bulletPosition;
	D3DXVECTOR2 bulletSize;
	D3DXVECTOR2 bulletDirection;
	BULLET_TYEP bulletType;
	BULLET_BELONG bulletBelong;
	float bulletSpeed;
	int bulletColor;	//0: red. 1.blue
	int damage;
};

void bulletInit();
void bulletUninit();
void bulletUpdate();
void bulletDraw();
void bulletReset();
void bulletCreate(D3DXVECTOR2 position, D3DXVECTOR2 direction, BULLET_TYEP type, BULLET_BELONG belong, float speed, int color, int damage);
void bulletProcess();

