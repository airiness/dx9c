#include <Windows.h>
#include <cassert>
#include <d3d9.h>
#include <d3dx9.h>
#include "enemy.h"
#include "player.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "scene.h"
#include "title.h"
#include "game.h"
#include "highscore.h"

static SCENE g_NextScene;
static SCENE g_Scene = g_NextScene;

void sceneInit()
{
	switch (g_NextScene)
	{
	case SCENE_TITLE:
		titleInit();
		break;
	case SCENE_GAME:
		gameInit();
		break;
	case SCENE_HIGHSCORE:
		highscoreInit();
		break;
	case SCENE_MAX:
		break;
	default:
		break;
	}

	assert(Texture_Load() > 0);
}

void sceneUninit()
{
}

void sceneUpdate()
{
	switch (g_Scene)
	{
	case SCENE_TITLE:
		titleUpdate();
		break;
	case SCENE_GAME:
		gameUpdate();
		break;
	case SCENE_HIGHSCORE:
		highscoreUpdate();
		break;
	case SCENE_MAX:
		break;
	default:
		break;
	}
}

void sceneDraw()
{

	switch (g_Scene)
	{
	case SCENE_TITLE:
		titleDraw();
		break;
	case SCENE_GAME:
		gameDraw();
		break;
	case SCENE_HIGHSCORE:
		highscoreDraw();
		break;
	case SCENE_MAX:
		break;
	default:
		break;
	}
}

void sceneSetNextScene(SCENE nextScene)
{
	g_NextScene = nextScene;
}

bool sceneChangeScene()
{
	if (true)
	{
		sceneUninit();
		sceneInit();
	}

	return false;
}
