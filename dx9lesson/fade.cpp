#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "fade.h"

D3DCOLOR g_FadeColor = 0;
bool g_FadeOut = false;
bool g_bFade = false;
int g_FadeFrame = 0;
int g_FadeFrameCount = 0;
int g_FadeStartFrame = 0;

void fadeInit()
{

}
bool fadeUpdate()
{
	g_FadeFrameCount++;
	if (g_bFade)
	{
		int elapsedFrame = g_FadeFrameCount - g_FadeStartFrame;

		float e = (float)elapsedFrame / g_FadeFrame;
		if (e > 1.0f)
		{
			g_bFade = false;
		}
		float alpha = g_FadeOut ? e : 1.0f - e;
		D3DXCOLOR color = g_FadeColor;
		color.a = alpha;
		g_FadeColor = color;
	}

	return !g_bFade;
}

void fadeUninit()
{

}

void fadeStart(int frame, D3DCOLOR color, bool bOut)
{
	g_FadeColor = color;
	g_FadeFrame = frame;
	g_FadeStartFrame = g_FadeFrameCount;
	g_FadeOut = bOut;
	g_bFade = true;
}

void fadeDraw()
{
	D3DXCOLOR color = g_FadeColor;
	if (color.a < 0.0001f)
	{
		return;
	}
	FadeVertex v[] =
	{
		{D3DXVECTOR4{0.0f,0.0f,0.0f,1.0f},g_FadeColor},
		{D3DXVECTOR4{SCREEN_WIDTH,0.0f,0.0f,1.0f},g_FadeColor},
		{D3DXVECTOR4{0.0f,SCREEN_HEIGHT,0.0f,1.0f},g_FadeColor},
		{D3DXVECTOR4{SCREEN_WIDTH,SCREEN_HEIGHT,0.0f,1.0f},g_FadeColor},
	};

	Direct3DGetDevice()->SetFVF(FVF_FADE_VERTEX);
	Direct3DGetDevice()->SetTexture(0, nullptr);
	Direct3DGetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, v, sizeof(FadeVertex));

}
