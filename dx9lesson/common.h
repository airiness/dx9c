#pragma once

void Init(HWND hWnd);
void Uninit(void);
void Update(void);
void Draw(void);
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);