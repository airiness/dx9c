#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "player.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "score.h"

static D3DXVECTOR2 g_score_position;
static D3DXVECTOR2 g_score_size;
static int g_score_textureID;
static int g_score;

void numberDraw(int n,float x, float y)
{

	Sprite_Draw(g_score_textureID, x, y, g_score_size.x * n,0, g_score_size.x, g_score_size.y);
}

void scoreInit()
{
	g_score_textureID = Texture_SetLoadFile("Asset/Texture/number.png", 640, 400);
	Texture_Load();
	g_score_position = D3DXVECTOR2(SCREEN_WIDTH * 0.9f, SCREEN_HEIGHT * 0.9f);
	g_score_size = { 32.0f,32.0f };

	scoreReset();
}

void scoreUninit()
{

}

void scoreUpdate()
{

}

void scoreDraw(int digit, bool bZero, bool bLeft)
{
	int score = g_score;
	float x = g_score_position.x;
	int n = score % 10;
	for (size_t i = 0; i < digit; i++)
	{
		numberDraw(n, x, g_score_position.y);
		score /= 10;
		if (score == 0)
		{
			break;
		}
		n = score % 10;
		x -= g_score_size.x;
	}
}

void scoreReset()
{
	g_score = 0;
}

void Addscore(int n)
{
	g_score += n;
	if (g_score<=0)
	{
		g_score = 0;
	}
}


