#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "bullet.h"
#include "player.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "collision.h"
#include "player.h"
#include "enemy.h"
#include "explosion.h"
#include "score.h"

const size_t g_bullet_max = 200;
BULLET g_bullets[g_bullet_max];
size_t g_bullet_textureID;

D3DCOLOR g_bullet_red = D3DCOLOR_RGBA(255, 0, 0, 255);
D3DCOLOR g_bullet_blue = D3DCOLOR_RGBA(0, 0, 255, 255);

const D3DXVECTOR2 g_bullet_texture_size = { 16.0f,16.0f };
const D3DXVECTOR2 g_bullet_size_default = { 12.0f,12.0f };

extern D3DXVECTOR2 g_player_position;
extern D3DXVECTOR2 g_player_size;
extern bool g_player_isUse;
extern int g_player_damage;
extern int g_player_bullet_type;
extern ENEMY g_enemies[ENEMY_MAX];

void bulletInit() {
	g_bullet_textureID = Texture_SetLoadFile("Asset/Texture/bullet.png", g_bullet_texture_size.x, g_bullet_texture_size.y);
	bulletReset();
}

void bulletUninit() {

}

void bulletUpdate() {

	for (size_t i = 0; i < g_bullet_max; i++)
	{
		if (g_bullets[i].bulletIsUse)
		{
			switch (g_bullets[i].bulletType)
			{
			case Bullet_type_line:
				break;
			case Bullet_type_sin:
				break;
			case Bullet_type_follow:
				break;
			default:
				break;
			}


			D3DXVec2Normalize(&g_bullets[i].bulletDirection, &g_bullets[i].bulletDirection);
			g_bullets[i].bulletPosition += g_bullets[i].bulletDirection * g_bullets[i].bulletSpeed;

			if ((g_bullets[i].bulletPosition.x > SCREEN_WIDTH+g_bullets[i].bulletSize.x) || 
				(g_bullets[i].bulletPosition.x <  - g_bullets[i].bulletSize.x) ||
				(g_bullets[i].bulletPosition.y > SCREEN_HEIGHT + g_bullets[i].bulletSize.y) ||
				(g_bullets[i].bulletPosition.y < -g_bullets[i].bulletSize.y))

			{
				g_bullets[i].bulletIsUse = false;
			}

		}
	}

	bulletProcess();

}

void bulletDraw() {
	for (size_t i = 0; i < g_bullet_max; i++)
	{
		if (g_bullets[i].bulletIsUse) {
			if (g_bullets[i].bulletColor == 0)
			{
				SetColor(g_bullet_red);
			}
			else if (g_bullets[i].bulletColor == 1)
			{
				SetColor(g_bullet_blue);
			}
			Sprite_Draw(g_bullet_textureID, g_bullets[i].bulletPosition.x, g_bullets[i].bulletPosition.y);
			SetColor(D3DCOLOR_RGBA(255, 255, 255, 255));
		}
	}
}

void bulletReset()
{
	for (size_t i = 0; i < g_bullet_max; i++)
	{
		g_bullets[i].bulletIsUse = false;
		g_bullets[i].bulletPosition = {0.0f,0.0f};
		g_bullets[i].bulletSize = g_bullet_size_default;
		g_bullets[i].bulletDirection = {0.0f,0.0f};
		g_bullets[i].bulletType = Bullet_type_none;
		g_bullets[i].bulletBelong = Bullet_belong_noone;
		g_bullets[i].bulletSpeed = 0.0f;
		g_bullets[i].bulletColor = 0;		//0: red. 1.blue
		g_bullets[i].damage = 0;
	}
}

void bulletCreate(D3DXVECTOR2 position,D3DXVECTOR2 direction,BULLET_TYEP type,BULLET_BELONG belong,float speed,int color,int damage) 
{
	for (size_t i = 0; i < g_bullet_max; i++)
	{
		if (!g_bullets[i].bulletIsUse)
		{
			g_bullets[i].bulletIsUse = true;
			g_bullets[i].bulletPosition = position;
			//bullets[i].bulletSize = { 12.0f,12.0f };
			g_bullets[i].bulletDirection = direction;
			g_bullets[i].bulletType = type;
			g_bullets[i].bulletBelong = belong;
			g_bullets[i].bulletSpeed = speed;
			g_bullets[i].bulletColor = color;		//0: red. 1.blue
			g_bullets[i].damage = damage;
			break;
		}
	}
}

void bulletProcess()
{

	for (int i = 0; i < g_bullet_max; i++)
	{
		if (g_bullets[i].bulletIsUse)
		{
			if (g_bullets[i].bulletBelong == Bullet_belong_player)
			{
				for (int j = 0;  j < ENEMY_MAX;  j++)
				{
					if (g_enemies[j].enemyIsUse)
					{
						Circle circleenemy = { g_enemies[j].enemyPosition,g_enemies[j].enemySize.x / 2.0f -5.0f };
						Circle circlebullet = { g_bullets[i].bulletPosition,g_bullets[i].bulletSize.x / 2.0f};
						if (IsHitCollsion_Circle_Circle(&circleenemy,&circlebullet))
						{
							if (g_bullets[i].bulletColor == g_enemies[j].enemyBulletType)
							{
								Addscore(-1);
							}
							else
							{
								g_enemies[j].enemyHP -= g_player_damage;
								SetExplosion(g_enemies[j].enemyPosition.x, g_enemies[j].enemyPosition.y);
								Addscore(g_enemies[j].enemyType);
							}
							g_bullets[i].bulletIsUse = false;

						}
					}
				}
			}
			else if (g_bullets[i].bulletBelong == Bullet_belong_enemy)
			{
				Circle circleplayer = { g_player_position,g_player_size.x / 2.0f };
				Circle circlebulletenemy = { g_bullets[i].bulletPosition,g_bullets[i].bulletSize.x / 2.0f };
				if (IsHitCollsion_Circle_Circle(&circleplayer, &circlebulletenemy))
				{
					if (g_bullets[i].bulletColor == g_player_bullet_type)
					{
						Addscore(1);
					}
					else 
					{
						SetExplosion(g_player_position.x, g_player_position.y);
						g_player_isUse = false;
					}
					g_bullets[i].bulletIsUse = false;
				}
			}
		}
	}
}
