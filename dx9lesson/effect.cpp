#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "enemy.h"
#include "main.h"
#include "sprite.h"
#include "texture.h"
#include "direct3d.h"
#include "effect.h"

size_t g_effect_textureID;
const int EFFECT_MAX = 100;
EFFECT effects[EFFECT_MAX];
D3DXVECTOR2 g_effect_size;
const D3DXVECTOR2 g_effect_texture_size = {128.0f,128.0f};

void effectInit()
{
	g_effect_size = g_effect_texture_size;
	g_effect_textureID = Texture_SetLoadFile("Asset/Texture/effect000.jpg", g_effect_texture_size.x, g_effect_texture_size.y);
	effectReset();
}

void effectUninit()
{
}

void effectUpdate()
{
	for (size_t i = 0; i < EFFECT_MAX; i++)
	{
		if (effects[i].effectFrame < 0)
		{
			continue;
		}

		if (effects[i].effectFrame > effects[i].effectLife)
		{
			effects[i].effectFrame = -1;
			continue;
		}

		float e = float(effects[i].effectFrame) / float(effects[i].effectLife);

		D3DXCOLOR color = effects[i].effectColor;
		color.a = 1.0f - e;
		effects[i].effectColor = color;
		effects[i].effectFrame++;
	}
}

void effectDraw()
{

	// レンダーステート(加算合成処理)
	// 色 = (SRC色 * SRCアルファ) + (DEST色 * (1.0)アルファ)
	Direct3DGetDevice()->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);		// ブレンディング処理(デフォルト)
	Direct3DGetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	// αソースカラーの指定
	Direct3DGetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);		// αデスティネーションカラーの指定(1.0)


	for (size_t i = 0; i < EFFECT_MAX; i++)
	{
		if (effects[i].effectFrame < 0)
		{
			continue;
		}
		SetColor(effects[i].effectColor);
		Sprite_Draw(g_effect_textureID, effects[i].dx, effects[i].dy,int(g_effect_size.x),int(g_effect_size.y));
	}
	// レンダーステート(通常ブレンド処理)
	// 色 = (SRC色 * SRCアルファ) + (DEST色 * (1.0 - SRC)アルファ)
	Direct3DGetDevice()->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);		// ブレンディング処理(デフォルト)
	Direct3DGetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	// αソースカラーの指定
	Direct3DGetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// αデスティネーションカラーの指定(1.0 - SRC)

	SetColor(D3DCOLOR_RGBA(255, 255, 255, 255));
}

void effectReset()
{
	for (size_t i = 0; i < EFFECT_MAX; i++)
	{
		effects[i].dx = 0.f;
		effects[i].dy = 0.f;
		effects[i].effectLife = 0;
		effects[i].effectFrame = -1;
		effects[i].effectScale = 1.0f;
		effects[i].effectColor = D3DCOLOR_RGBA(0, 0, 0, 0);
	}
}

void effectCreate(float x, float y, D3DCOLOR color, int life, float scale)
{
	for (int i = 0; i < EFFECT_MAX; i++)
	{
		if (effects[i].effectFrame >= 0)
		{
			continue;
		}

		effects[i].dx = x;
		effects[i].dy = y;
		effects[i].effectLife = life;
		effects[i].effectFrame = 0;
		effects[i].effectScale = scale;
		effects[i].effectColor = color;
		break;
	}
}
